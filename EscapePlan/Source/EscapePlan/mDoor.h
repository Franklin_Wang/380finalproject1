// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "mDoor.generated.h"

UCLASS()
class ESCAPEPLAN_API AmDoor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AmDoor();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

protected:
    
    UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = Utilities) UParticleSystemComponent* DoorMesh;
 //   UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = Utilities) USkeletalMeshComponent* DoorMesh;

	void TeleportPlayer();
	
};
