// Fill out your copyright notice in the Description page of Project Settings.

#include "EscapePlan.h"
#include "LockDoor.h"
#include "EscapePlanCharacter.h"


// Sets default values
ALockDoor::ALockDoor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	DoorMesh = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Door"));
	RootComponent = DoorMesh;
}

// Called when the game starts or when spawned
void ALockDoor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ALockDoor::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
	TeleportPlayer();
}

void ALockDoor::TeleportPlayer()
{
	APawn* playerPawn = UGameplayStatics::GetPlayerPawn(this, 0);
	AActor* door = this;
	if (playerPawn && door)
	{
		AEscapePlanCharacter* player = Cast<AEscapePlanCharacter>(playerPawn);
		float dis = FVector::Dist(player->GetActorLocation(), door->GetActorLocation());
		if (dis > -150.0f && dis < 150.0f && player->getkey())
		{
			FVector playLoc = player->GetActorLocation();
			if (playLoc.Y > door->GetActorLocation().Y)
			{
				playLoc.Y -= 300.0f;
			}
			else
			{
				playLoc.Y += 300.0f;
			}
			player->SetActorLocation(playLoc, false);
		}
		else if (dis > -150.0f && dis < 150.0f && !player->getkey())
		{
			FVector playLoc = player->GetActorLocation();
			if (playLoc.Y > door->GetActorLocation().Y)
			{
				playLoc.Y += 200.0f;
			}
			else
			{
				playLoc.Y -= 200.0f;
			}
			player->SetActorLocation(playLoc, false);
		}
	}
}

