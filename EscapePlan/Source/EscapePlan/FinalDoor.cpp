// Fill out your copyright notice in the Description page of Project Settings.

#include "EscapePlan.h"
#include "FinalDoor.h"
#include "EscapePlanCharacter.h"


// Sets default values
AFinalDoor::AFinalDoor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	DoorMesh = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Door"));
	RootComponent = DoorMesh;

}

// Called when the game starts or when spawned
void AFinalDoor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFinalDoor::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
	checkCollision();
}

void AFinalDoor::checkCollision() {
	APawn* playerPawn = UGameplayStatics::GetPlayerPawn(this, 0);
	AActor* door = this;
	if (playerPawn && door)
	{
		AEscapePlanCharacter* player = Cast<AEscapePlanCharacter>(playerPawn);
		float dis = FVector::Dist(player->GetActorLocation(), door->GetActorLocation());
		if (dis > -150.0f && dis < 150.0f && player->getkey())
		{
			player->Destroy();
		}
		
	}
}
