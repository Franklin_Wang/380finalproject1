// Fill out your copyright notice in the Description page of Project Settings.

#include "EscapePlan.h"
#include "ZombieAIController.h"
#include "Zombie.h"

void AZombieAIController::BeginPlay()
{
	Super::BeginPlay();
	APawn* pawn = UGameplayStatics::GetPlayerPawn(this, 0);
	MoveToActor(pawn);
	state = Start; 
	attackRange = 150.0f;
}

void AZombieAIController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	switch (state)
	{
	case AZombieAIController::Start:
		if (UGameplayStatics::GetPlayerPawn(this, 0))
			MoveToActor(UGameplayStatics::GetPlayerPawn(this, 0));
		state = Chase;
		break;
	case AZombieAIController::Chase:
		if (UGameplayStatics::GetPlayerPawn(this, 0))
			MoveToActor(UGameplayStatics::GetPlayerPawn(this, 0));
		break;
	case AZombieAIController::Attack:
		pawnPlayer = UGameplayStatics::GetPlayerPawn(this, 0);
		pawnEnemy = GetControlledPawn();
		if (pawnEnemy != nullptr && pawnPlayer != nullptr) {
			dis = FVector::Dist(pawnPlayer->GetActorLocation(), pawnEnemy->GetActorLocation());
			if (dis > attackRange) {
				MoveToActor(pawnPlayer);
				state = Chase;
				
					//if (pawnEnemy->IsA<AZombie>)
					//{
						AZombie* zombie = Cast<AZombie>(pawnEnemy);
						zombie->StopAttack();
					//}
					//if (pawnEnemy->IsA<AEnemy>)
					//{
						//AEnemy* dwarf = Cast<AEnemy>(pawnEnemy);
						//dwarf->StopAttack();
					//}
			}
		}

		break;
	case AZombieAIController::Dead:
		state = Dead;
		break;
	default:
		break;
	}
}

void AZombieAIController::OnMoveCompleted(FAIRequestID RequestID, EPathFollowingResult::Type Result)
{
	if (Result == EPathFollowingResult::Success) {
		state = Attack;
		//AEnemy* dwarf = Cast<AEnemy>(GetPawn());
		APawn* pawn = GetControlledPawn();
		//if (pawn)
		//{
			
				AZombie* zombie = Cast<AZombie>(pawn);
				zombie -> StartAttack();
			
				//AEnemy* dwarf = Cast<AEnemy>(pawn);
				//dwarf->StartAttack();
		//}
	}
}


