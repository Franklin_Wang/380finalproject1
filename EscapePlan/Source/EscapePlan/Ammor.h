// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Ammor.generated.h"

UCLASS()
class ESCAPEPLAN_API AAmmor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AAmmor();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;
    
    void checkCollision();

protected:
    
    UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = Weapon) UStaticMeshComponent* AmmoMesh;
	
};
