// Fill out your copyright notice in the Description page of Project Settings.

#include "EscapePlan.h"
#include "mRealkey.h"
#include "EscapePlanCharacter.h"


// Sets default values
AmRealkey::AmRealkey()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AmRealkey::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AmRealkey::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
	checkCollision();
}

void AmRealkey::checkCollision() 
{
	//UGameplayStatics::GetPlayerCharacter(this, 0)
	AEscapePlanCharacter* player = Cast<AEscapePlanCharacter>(UGameplayStatics::GetPlayerPawn(this, 0));
	AActor* mKey = this;
	if (player && mKey)
	{
		float dis = FVector::Dist(player->GetActorLocation(), mKey->GetActorLocation());
		if (dis > -100.0 && dis < 100.0f)
		{
			player->setkey();
			Destroy();
		}
	}
}